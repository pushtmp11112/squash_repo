FROM accetto/ubuntu-vnc-xfce:latest
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update
RUN apt-get install -y git nano neofetch curl wget vim unzip sudo openssh-server screen
RUN wget https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-linux-amd64.zip -O /root/ngrok.zip
RUN unzip /root/ngrok.zip -d /root
RUN /root/ngrok authtoken 21rDy2b3yxkn3WDTWglFzh9cLKV_5k5MMqcQxTy1EfHkbYVCn